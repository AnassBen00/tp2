package fr.uavignon.ceri.tp2;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp2.data.Book;

@Database(entities = {Book.class}, version = 1)
public abstract class BookRoomDatabase extends RoomDatabase {

    public abstract BookDao bookDao();
    private static volatile BookRoomDatabase bookRoomDatabase;

    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(5);

    static BookRoomDatabase getDatabase(final Context context) {
        if (bookRoomDatabase == null) {
            synchronized (BookRoomDatabase.class) {
                if (bookRoomDatabase == null) {
                    bookRoomDatabase = Room.databaseBuilder(context.getApplicationContext(),
                                    BookRoomDatabase.class,
                                    "books_database").build();
                }
            }
        }
        return bookRoomDatabase;
    }



}
