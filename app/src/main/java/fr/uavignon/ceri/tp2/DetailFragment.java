package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    private DetailViewModel viewModel;

    private RecyclerAdapter adapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        textTitle = getView().findViewById(R.id.nameBook);
        textAuthors = getView().findViewById(R.id.editAuthors);
        textYear = getView().findViewById(R.id.editYear);
        textGenres = getView().findViewById(R.id.editGenres);
        textPublisher = getView().findViewById(R.id.editPublisher);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        long bookID = args.getBookNum();
        viewModel.setBook(bookID);

        if (bookID != -1){
            viewModel.getSelected().observe(getViewLifecycleOwner(),
                    new Observer<Book>() {
                        @Override
                        public void onChanged(@Nullable final Book book) {
                            textTitle.setText(book.getTitle());
                            textAuthors.setText(book.getAuthors());
                            textYear.setText(book.getYear());
                            textGenres.setText(book.getGenres());
                            textPublisher.setText(book.getPublisher());
                        }
                    });
        }else {
            clearFields();
        }

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        Button updateButton = getView().findViewById(R.id.buttonUpdate);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = textTitle.getText().toString();
                String Author = textAuthors.getText().toString();
                String Year = textYear.getText().toString();
                String genre = textGenres.getText().toString();
                String publisher = textPublisher.getText().toString();
                if (!title.equals("") && !Author.equals("") && !Year.equals("") && !genre.equals("") && !publisher.equals("")) {
                    Book book = new Book(title, Author, Year, genre, publisher);

                    if(viewModel.getSelected().getValue()!= null )  book.setId(viewModel.getSelected().getValue().getId());

                    //System.out.println("listener"+book.getId());
                    viewModel.insertOrUpdate(book);
                    clearFields();
                }
                else {
                    Snackbar snackbar = Snackbar
                            .make(view, "Vous devez remplir tous les champs", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }

    private void clearFields() {
        textTitle.setText("");
        textAuthors.setText("");
        textYear.setText("");
        textGenres.setText("");
        textPublisher.setText("");
    }
}