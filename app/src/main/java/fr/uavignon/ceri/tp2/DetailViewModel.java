package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel {

    private BookRepository repository;
    private MutableLiveData selected;


    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        selected = repository.getSelectedBook();
    }

    public MutableLiveData<Book> getSelected(){
        return selected;
    }


    public void insertOrUpdate(Book book ){
        if (book.getId()!=0) {
            repository.updateBook(book);
        } else {
            repository.insertBook(book);
        }
    }

    public void deleteBook(long id){
        repository.deleteBook(id);
    }

    public void setBook(long bookId) {
        repository.getBook(bookId);
        selected = repository.getSelectedBook();
    }

    public void getBook(long id){
         repository.getBook(id);
    }

}
